<?php
require_once('loadclasses.php');
$page = new Page('About');
$html = '<p>Eve mining fleet/reprocessing calculator &copy;'.date('Y').' Snitch Ashor, idea and concept by Panda Kos.<br/><br/>
Version 0.1<br/><br/>
This app is built using php and the <a href="http://getbootstrap.com/">bootstrap</a> framework.<br/>
All interactions with EVE Online are done using the <a href="https://esi.evetech.net/">EVE Swagger Interface</a><br/>
<br/>
Additional Software used:<br/>
<ul>
<li>ESI php client generated with <a href="http://swagger.io/swagger-codegen/">swagger-codegen</a></li>
<li>Auth was adopted from Fuzzy Steve\'s <a href="https://github.com/fuzzysteve/eve-sso-auth">EVE SSO Auth</a></li>
<li><a href="https://jquery.com/">jQuery</a></li>
<li>jQuery <a href="https://datatables.net/">datatables</a></li>
<li>Twitter <a href="https://twitter.github.io/typeahead.js/">typeahead.js</a></li>
<li><a href="http://searchturbine.com/php/phpwee">PHPWee</a> Minifier</li>
</ul>
<br/>
So long,<br/>
o7, Snitch.
</p>
';
$page->addBody($html);
$page->display();
exit;
?>

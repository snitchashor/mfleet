<?php
require_once('config.php');
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

use Swagger\Client\Configuration;
use Swagger\Client\ApiException;

require_once('classes/esi/autoload.php');
require_once('classes/class.esisso.php');

class ESIASSETS extends ESISSO
{

        public function __construct($characterID) {
            parent::__construct(null, $characterID);
        }
         
        public function getAssetApi($scope = 'esi-assets.read_assets.v1') {
            $accessToken = $this->getAccessToken($scope);
            $esiapi = new ESIAPI();
            $esiapi->setAccessToken($accessToken);
            $assetapi = $esiapi->getApi('Assets');
            return $assetapi;
        }

        public function getAssets() {
            $assetapi = $this->getAssetApi();
            $assets = array();
            try {
                $fetch = $assetapi->getCharactersCharacterIdAssetsWithHttpInfo($this->characterID, 'tranquility');
            } catch (Exception $e) {
                $this->error = true;
                $this->message = 'Could not retrieve Assets: '.$e->getMessage().PHP_EOL;
                $this->log->exception($e);
                return null;
            }
            $result = $fetch[0];
            $pages = ($fetch[2]['X-Pages'][0]);
            if ($pages > 1) {
                $i = 2;
                $promises = array();
                do {
                    $promises[] = $assetapi->getCharactersCharacterIdAssetsAsync($this->characterID, 'tranquility', null, (string)$i);
                    $i += 1;
                } while ($i <= $pages);
                $responses = GuzzleHttp\Promise\settle($promises)->wait();
                foreach ($responses as $response) {
                    if($response['state'] == 'fulfilled') {
                        $result = array_merge($result, ($response['value']));
                    }
                }
            }
            if (!count($result)) {
                return null;
            }
            $this->log->put(count($result));
            $typeids = array();
            $locationids = array(); 
            foreach($result as $r) {
                $l = $r->getLocationId();
                if (!isset($locationids[$l])) {
                    $locationids[$l] = null;
                }
                $i = $r->getTypeId();
                if (!isset($typeids[$i])) {
                    $typeids[$i] = null;
                }
            }
            foreach($result as $r) {
                $l = $r->getLocationId();
                $i = $r->getTypeId();
                if ($r->getLocationFlag() == 'Hangar' || $r->getLocationFlag() == 'HangarAll') {
                    if (!isset($assets[$l])) {
                        $assets[$l] = array();
                        $assets[$l]['hangar'] = array();
                    }
                    if (isset($assets[$l]['hangar'][$i])) {
                        $assets[$l]['hangar'][$i]['qty'] += $r->getQuantity();
                    } else {
                        $assets[$l]['hangar'][$i] = ['qty' => $r->getQuantity()];
                    }
                }
            }
            $stationnames = EVEHELPERS::getStationNames(array_keys($assets));
            $structures = array();
            foreach (array_keys($assets) as $id) {
                if (isset($stationnames[$id])) {
                    $assets[$id]['name'] = $stationnames[$id];
                } else {
                    $structures[$id] = 'Unknown structure';
                }
            }
            if (count($structures)) {
                $esiapi = new ESIAPI();
                $esiapi->setAccessToken($this->getAccessToken('esi-universe.read_structures.v1'));
                $universeapi = $esiapi->getApi('Universe');
                $promises = array();
                foreach (array_keys($structures) as $id) {
                    $promises[$id] = $universeapi->getUniverseStructuresStructureIdAsync($id, 'tranquility');
                }
                $responses = GuzzleHttp\Promise\settle($promises)->wait();
                foreach ($responses as $id => $response) {
                    if($response['state'] == 'fulfilled') {
                        $structures[$id] = $response['value']->getName();
                    }
                }
                foreach ($structures as $id => $name) {
                    $assets[$id]['name'] = $name;
                }
            }
            $this->log->put(json_encode($assets));
            return $assets;
        }
}

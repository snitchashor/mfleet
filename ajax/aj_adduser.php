<?php
if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

chdir(str_replace('/ajax','', getcwd()));
require_once('config.php');
require_once('loadclasses.php');

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
  if(@isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']==str_replace('/ajax','',URL::url_path().'admin.php'))
  {
    if(($_POST['ajtok'] == $_SESSION['ajtoken']) && (isset($_POST['userid'])) && (isset($_POST['username'])) && (isset($_POST['usertype'])) && ($_SESSION['isAdmin'] == True)) {
      $qry = DB::getConnection();
      $sql = "SELECT * FROM allowed_users WHERE id=".$_POST['userid'];
      $result = $qry->query($sql);
      if ($result->num_rows) {
        echo('User already added.');
        exit;
      } else {
        if ($stmt = $qry->prepare("INSERT INTO allowed_users (id, name, type) VALUES (?, ?, ?)")) {
          $stmt->bind_param('iss', $userid, $name, $type);
          $userid = $_POST['userid'];
          $name = $_POST['username'];
          $type = $_POST['usertype'];
          $stmt->execute();
          if ($stmt->errno) {
              echo($stmt->error);
              $stmt->close();
              exit;
          }
          $stmt->close();
          echo('User added.');
          exit;
        } else {
          echo('false');
          exit;
        }
      }
    }
    else {
      echo('false');
      exit;
    }
  }
  else {
    echo('false');
    exit;
  }
} else {
  echo('false');
  exit;
}
?>

<?php
$start_time = microtime(true);
require_once('auth.php');
require_once('config.php');
require_once('loadclasses.php');

$start_time = microtime(true);

if (session_status() != PHP_SESSION_ACTIVE) {
  header('Location: '.URL::url_path.'index.php');
  die();
}

if (!isset($_SESSION['isAdmin']) || !$_SESSION['isAdmin']) {
  header('Location: '.URL::url_path().'index.php');
  die();
}

if (!isset($_SESSION['ajtoken'])) {
  $_SESSION['ajtoken'] = EVEHELPERS::random_str(32);
}

if (isset($_POST['clearcache'])) {
    $fi = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('cache/'), RecursiveIteratorIterator::SELF_FIRST);
    foreach ($fi as $file) {
        if ($file->isFile() and (substr($file->getFilename(), 0, 1) != '.' ) ) {
            unlink($file->getRealPath());
        }
    }
    $di = new DirectoryIterator('cache/api/');
    foreach ($di as $dir) {
        if ($dir->isDir() and (substr($dir->getFilename(), 0, 1) != '.' ) ) {
            rmdir($dir->getRealPath());
        }
    }
}

$qry = DB::getConnection();
$sql="SELECT * FROM esisso";
$users = $qry->query($sql)->num_rows;
$sql="SELECT * FROM accessTokens WHERE expires > DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY characterID";
$users24h = $qry->query($sql)->num_rows;

$esierrors1h = 0;
$esierrors24h = 0;
$logtext = [];
(is_file('log/esi.log')?$nolog = false:$nolog = true);
if (!$nolog) {
    $handle = fopen('log/esi.log','r');
    while (!feof($handle)) {
        $dd = fgets($handle);
        $temp = [];
        if (strlen($dd) > 20) {
            $arr = explode(" ", $dd);
            $temp = [];
            $arr = explode(" ", fgets($handle, 4096));
            if (count($arr) >= 4) {
                $temp['date'] = $arr[0];
                $temp['time'] = $arr[1];
                $temp['type'] = $arr[2];
                $temp['message'] = implode(" ", array_slice($arr,3));
                $logtext[] = $temp;
            }
            $timestamp = substr($dd, 0, 20);
            $time = strtotime($timestamp);
            if($time > strtotime("-1 hours")) {
                $esierrors1h += 1;
                $esierrors24h += 1;
            } elseif ($time > strtotime("-24 hours")) {
                $esierrors24h += 1;
            }
        }
    }
    fclose($handle);
    array_reverse($logtext);
}


$fi = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('cache/'), RecursiveIteratorIterator::SELF_FIRST);
$cachecount = 0;
$cachesize = 0;
foreach ($fi as $file) {
    if ($file->isFile() and (substr($file->getFilename(), 0, 1) != '.' ) ) {
        $cachecount += 1;
        $cachesize += $file->getSize();
    }
}

$allowed_users = DBH::getAllowedUsers();

$html = '<div class="row">
             <div class="col-sm-12"><h3>Allowed Users</h3>
               <div class="row"><div class="col-sm-12 col-md-7 col-lg-5">
               <table class="small table table-striped table-condensed table-hover" cellspacing="0" width="100%">
                 <thead>
                   <tr>
                     <th class="col-xs-1"></th>
                     <th class="col-xs-7">Name</th>
                     <th class="col-xs-3">Type</th>
                     <th class="col-xs-1"></th>
                   </tr>
                 </thead>
                 <tbody>';
                 foreach($allowed_users as $u) {
                   $html .= '<tr id="'.$u['id'].'"><td><img height="24px" src="https://imageserver.eveonline.com/'.ucfirst($u['type']).'/'.$u['id'].'_32.'.($u['type']=='character'?'jpg':'png').'"></td>';
                   $html .= '<td class="name">'.$u['name'].'</td>';
                   $html .= '<td>'.ucfirst($u['type']).'</td>';
                   $html .= '<td><button type="button" class="btn btn-link btn-default btn-xs" onclick="deluser(this)"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
                 }
$html .= '       </tbody>
               </table>
               </div></div>
               <div class="tt-users form-group col-xs-11 col-sm-5 col-md-6 col-lg-3">
                   <label for="user-name" class="control-label">Add User:</label>
                   <input id="user-name" type="text" class="typeahead pilot form-control">
                   <div id="tt-loading-spinner" class="pull-right">
                      <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                   </div>
                   <input id="user-id" type="hidden" value="">
                   <input id="user-type" type="hidden" value="">
                   <button type="button" id="inv-button" class="tt-btn btn btn-primary disabled"><span class="glyphicon glyphicon-plus"></span></button>
               </div>
             </div>
             <div class="col-sm-12 col-md-6 col-lg-4"><h3>Statistics</h3>
               <div class="well well-sm">
                 <div class="row">
                     <div class="col-sm-7 col-lg-9">
                         Total users:
                     </div>
                     <div class="col-sm-4 col-lg-2 text-right">
                         '.$users.'
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-7 col-lg-9">
                         Users in the last 24h:
                     </div>
                     <div class="col-sm-4 col-lg-2 text-right">
                         '.$users24h.'
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-7 col-lg-9">
                         ESI errors (last hour):
                     </div>
                     <div class="col-sm-4 col-lg-2 text-right">
                         '.$esierrors1h.'
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-7 col-lg-9">
                         ESI errors (last 24 hours):
                     </div>
                     <div class="col-sm-4 col-lg-2 text-right">
                         '.$esierrors24h.'
                     </div>
                 </div>
               </div>
             </div>
             <div class="col-sm-12 col-md-6 col-lg-4"><h3>Cache</h3>
               <div class="well well-sm">
                 <div class="row">
                     <div class="col-sm-7 col-lg-8">
                         Number of Files:
                     </div>
                     <div class="col-sm-5 col-lg-4 text-right">
                         '.$cachecount.'
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-7 col-lg-8">
                         Cache Size:
                     </div>
                     <div class="col-sm-5 col-lg-4 text-right">
                         '.round($cachesize/(1024*1024), 2).' MB
                     </div>
                 </div>
               </div>
               <form id="cache" role="form" action="" method="post">
                   <button id="clearcache" name="clearcache" type="submit" value="clearcache" class="btn btn-primary pull-right">Clear Cache</button>
               </form>
             </div>
             <div class="col-lg-12"><h3>ESI error log</h3>
                 <div class="">
                     <table class="table table-striped small" id="logtable">
                       <thead>
                         <th>Date</th>
                         <th>Time</th>
                         <th class="wordbreak">Message</th>
                       </thead>
                       <tbody>';
foreach ($logtext as $l) {
    $html .= '<tr><td>'.$l['date'].'</td><td>'.$l['time'].'</td><td class="wrap">'.$l['message'].'</td></tr>';
}
                       
$html.=             ' </tbody>
                    </table>
                 </div>
             </div>
         </div>';

$footer = '<script>$(document).ready(function() {
            var table = $("#logtable").dataTable(
               {
                   "bPaginate": true,
                   "pageLength": 25,
                   "aoColumnDefs" : [ {
                       "bSortable" : false,
                       "aTargets" : [ "no-sort" ]
                   }, {
                       className: "wordbreak",
                       "aTargets" : [ "wordbreak" ]
                   } ],
                   fixedHeader: {
                       header: true,
                       footer: false
                   },
                   "order": [[ 0, "desc" ], [ 1, "desc" ]],
               });

               $( "#inv-button" ).click(function() {
                 var user_id = $("#user-id").val();
                 var user_name = $("#user-name").val();
                 var user_type = $("#user-type").val();
                 $.ajax({
                     type: "POST",
                     url: "'.URL::url_path().'ajax/aj_adduser.php",
                     data: {"ajtok" : "'.$_SESSION['ajtoken'].'", "userid" : user_id, "username" : user_name, "usertype" : user_type },
                     success:function(data) {
                         if (data == "false") {
                             BootstrapDialog.show({message: "Something went wrong...", type: BootstrapDialog.TYPE_WARNING});
                         } else {
                             BootstrapDialog.show({message: data, onhide: function(){location.reload();}});
                         }
                     }
                 });
               });
             });

             function deluser(btn) {
                 var row = btn.closest("tr");
                 var user_id = $(row).attr("id");
                 var name = $(row).children(".name").text();
                 BootstrapDialog.show({
                      message: "Are you sure you want to remove "+name+" from the list of Allowed users?",
                      buttons: [{
                          label: "Remove",
                          action: function(dialogItself){
                              dialogItself.close();
                              $.ajax({
                                  type: "POST",
                                  url: "'.URL::url_path().'ajax/aj_deluser.php",
                                  data: {"ajtok" : "'.$_SESSION['ajtoken'].'", "userid" : user_id},
                                  success:function(data) {
                                      if (data == "false") {
                                          BootstrapDialog.show({message: "Something went wrong...", type: BootstrapDialog.TYPE_WARNING});
                                      } else {
                                          BootstrapDialog.show({message: data, onhide: function(){location.reload();}});
                                      }
                                  }
                              });
                          }
                      },{
                          label: "Cancel",
                          action: function(dialogItself){
                              dialogItself.close();
                          }
                      }],
                 });
             }
         </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/dt-custom.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="js/esi_autocomplete.js"></script>
    <script src="js/bootstrap-dialog.min.js"></script>
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.min.css" integrity="sha256-C4J6NW3obn7eEgdECI2D1pMBTve41JFWQs0UTboJSTg=" crossorigin="anonymous" />';

$page = new Page('Admin Panel');
$page->addHeader('<link href="css/typeaheadjs.css" rel="stylesheet">');
$page->addBody($html);
$page->addFooter($footer);
$page->setBuildTime(number_format(microtime(true) - $start_time, 3));
$page->display();
exit;
?>

<?php

$start_time = microtime(true);

require_once('loadclasses.php');
require_once('auth.php');

$page = new Page($_SESSION['characterName'].'\'s Mining fleet');

function getForm($post = array('order_type' => 'buy_perc', 'rep_eff' => '80', 'tax' => '5', 'hub' => 'jita', 'payout' => 'volrep', 'removed' => array(), 'supporters' => 1, 'supp_share' => 5)) {
    $html = '<div id="placeholder"></div>
                 <form id="fleetlog" method="post" action="" role="form">';
    foreach ($post['removed'] as $r) {
        $html .= '<input type="hidden" name="removed[]" value="'.$r.'">';
    }
    $html .='<div class="row">
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-5 col-lg-2">
                     <label for="rep_eff">Rep. yield (%):</label>
                     <input type="number" id="rep_eff" class="form-control" name="rep_eff" value="'.$post['rep_eff'].'" style="width:65px">
                 </div>
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-5 col-lg-2">
                     <label for="tax">Tax (%):</label>
                     <input type="number" id="tax" class="form-control" name="tax" value="'.$post['tax'].'" style="width:65px">
                 </div>
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-5 col-lg-4">
                     <label for="order_type">Order type:</label>
                     <select id="order_type" name="order_type" class="form-control">
                         <option value="buy_perc"'.($post['order_type'] == "buy_perc"?' selected':'').'>Buy order (95th percentile)</option>
                         <option value="sell_perc"'.($post['order_type'] == "sell_perc"?' selected':'').'>Sell order (5th percentile)</option>
                         <option value="sell_low"'.($post['order_type'] == "sell_low"?' selected':'').'>Lowest sell order</option>
                         <option value="buy_high"'.($post['order_type'] == "buy_high"?' selected':'').'>Highest buy order</option>
                     </select>
                 </div>
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-3 col-lg-4">
                     <label for="hub">Trade hub:</label>
                     <select id="hub" name="hub" class="form-control">
                          <option value="jita"'.($post['hub'] == "jita"?' selected':'').'>Jita IV - 4</option>
                          <option value="amarr"'.($post['hub'] == "amarr"?' selected':'').'>Amarr - Emperor Family</option>
                          <option value="rens"'.($post['hub'] == "rens"?' selected':'').'>Rens IV - 8 Brutor Tribe</option>
                          <option value="dodixie"'.($post['hub'] == "dodixie"?' selected':'').'>Dodixie IX - 20 FedNavy</option>
                          <option value="hek"'.($post['hub'] == "hex"?' selected':'').'>Hek VIII - 12 Boundless</option>
                     </select>
                 </div>
             </div>
             <div class="row">
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-5 col-lg-3">
                     <label for="supporters"># of supporters:</label>
                     <input type="number" id="supporters" class="form-control" name="supporters" value="'.$post['supporters'].'" style="width:65px">
                 </div>
                 <div class="form-group form-inline col-xs-12 col-sm-6 col-md-5 col-lg-4">
                     <label for="supp_share">Supporters share each (% of total):</label>
                     <input type="number" id="supp_share" class="form-control" name="supp_share" value="'.$post['supp_share'].'" style="width:65px">
                 </div>
             </div>
             <div class="row">
                 <div class="form-group">
                 <label class="radio-inline">Payout calculation:</label>
                 <label class="radio-inline"><input type="radio" name="payout" value="volrep"'.($post['payout'] == "volrep"?' checked':'').'>Volume share (rep.)</label>
                 <label class="radio-inline"><input type="radio" name="payout" value="volraw"'.($post['payout'] == "volraw"?' checked':'').'>Volume share (Ore)</label>
                 <label class="radio-inline"><input type="radio" name="payout" value="repisk"'.($post['payout'] == "repisk"?' checked':'').'>Reprocessed value</label>
                 <label class="radio-inline"><input type="radio" name="payout" value="rawisk"'.($post['payout'] == "rawisk"?' checked':'').'>Raw ore Value</label>
                 </div>
             </div>
             <div class="row">
                 <div class="form-group col-xs-12 col-md-10 col-lg-8">
                    <label for="itemlist">Paste Mining log:</label>
                    <textarea id="itemlist" class="textarea form-control" rows="15" name="itemlist">'.(isset($post['itemlist'])?$post['itemlist']:'').'</textarea>
                 </div>
             </div>
             <div class="row">
                 <div class="col-xs-12 col-md-2 col-lg-1">
                     <button id="sendbutton" type="submit" name="submit" value="submit" class="btn btn-primary">Send</button>
                 </div>
                 <div class="col-xs-1 col-md-2 col-lg-1" style="margin-top: 4px; padding-left: 0;"><span id="loading2" style="display: none"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></span></div>
             </div>
         </form>';
    return $html;
}

function getScriptFooter() {
    $html = '<script>
        $(document).ready(readyFunction);
        function format(value) {
            var details = "";
            value.forEach(function callback(curval) {
                console.log(curval);
                details += "<div class=\'oreRow\' style=\'display:table-row;\'> \
                            <div style=\'display: table-cell;padding: 5px;\'>"+curval["amount"]+" x<\/div> \
                            <div style=\'display: table-cell;padding: 5px;text-align: left;\'><img class=\'img-rounded\' height=\'24px\' src=\'https://images.evetech.net/types/"+curval["id"]+"/icon?size=32\'>&nbsp"+curval["name"]+"<\/div> \
                            <div style=\'display: table-cell;padding: 5px;\'>"+$.number(curval["volume"], 0)+"m³<\/div> \
                            <div style=\'display: table-cell;padding: 5px;\'>"+$.number(curval["valuerep"]/100, 0)+" ISK reprocessed<\/div> \
                            <div style=\'display: table-cell;padding: 5px;\'>"+$.number(curval["value"]/100, 0)+" ISK raw value<\/div> \
                            <\/div>";
            });
            console.log(details);
            return "<div class=\'oreDetails text-muted\' style=\'display:table;margin-left: 5%;text-align: right;\'>"+details+"<\/div>";
        }
        function readyFunction() {
            var table = $(".jdatatable").DataTable(
               {
                   "bPaginate": true,
                   "aoColumnDefs" : [ {
                       "bSortable" : false,
                       "aTargets" : [ "no-sort" ]
                   }, {
                       "sClass" : "num-col",
                       "aTargets" : [ "num" ],
                   }, {
                       "aTargets" : [ "price" ],
                       render: $.fn.dataTable.render.number( ",", ".", 2 )
                   }, {
                       "aTargets" : [ "volume" ],
                       render: $.fn.dataTable.render.number( ",", ".", 0 )
                   }, {
                       "aTargets" : [ "payout" ],
                       render: function(data, type) {
                       if (type === "display") {
                           return "<button type=\'button\' class=\'btn btn-link btn-xs\' onclick=\'payoutclip("+data+")\' title=\'Copy to clipboard\'><i class=\'fa fa-clipboard\' aria-hidden=\'true\'><\/i><\/button>" + $.fn.dataTable.render.number( ",", ".", 2 ).display(data);
                       }
                       return data;
                       }
                   } ], 
                   "order": [[ 3, "desc" ]],
                   "pageLength": 25,
                   fixedHeader: {
                       header: true,
                       footer: true
                   },
                   responsive: false,
                   "footerCallback": function ( row, data, start, end, display ) {
                   var api = this.api(), data;
                     api.columns(".sumup", {
                       //page: "current"
                     }).every(function() {
                       var sum = this
                         .data()
                         .reduce(function(a, b) {
                           var x = parseFloat(a) || 0;
                           var y = parseFloat(b) || 0;
                           return x + y;
                         }, 0);
                       $(this.footer()).html( String(sum.toFixed(2)).split("").reverse().join("")
                  .replace(/(\d{3}\B)/g, "$1,")
                  .split("").reverse().join("") );
                     });
                   }
               });
            $(".jdatatable").on( "draw.dt", function () {
                markup();
            } );
            $(".jdatatable").on("click", "td.details-control", function () {
                var tr = $(this).closest("tr");
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass("shown");
                } else {
                    // Open this row
                    row.child(format(tr.data("child-value"))).show();
                    tr.addClass("shown");
                }
            });
        }

        function deluser(btn) {
            var row = btn.closest("tr");
            var user_id = $(row).attr("id");
            var name = $(row).children(".name").text();
            var flog = $("#fleetlog");
            BootstrapDialog.show({
                 message: "Are you sure you want to remove "+name+" from the calculation?",
                 buttons: [{
                     label: "Remove",
                     action: function(dialogItself){
                         dialogItself.close();
                         flog.append("<input type=\'hidden\' name=\'removed[]\' value=\'"+user_id+"\'>");
                         $("#sendbutton").trigger("click");
                     }
                 },{
                     label: "Cancel",
                     action: function(dialogItself){
                         dialogItself.close();
                     }
                 }],
            });
        }
        function payoutclip(value) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(value).select();
            document.execCommand("copy");
            $temp.remove();
        }

    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="js/bootstrap-dialog.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js" integrity="sha512-3z5bMAV+N1OaSH+65z+E0YCCEzU8fycphTBaOWkvunH9EtfahAlcJqAVN2evyg0m7ipaACKoVk6S9H2mEewJWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet">
    <link href="css/dt-custom2.css" rel="stylesheet">';
    return $html;
}


if ((isset($_POST['submit']) && strlen($_POST['itemlist']) > 10)) {
    if (!isset($_POST['removed'])){
        $_POST['removed'] = array();
    }
    $removed = array_unique($_POST['removed']);
    $rep_eff = (float)$_POST['rep_eff']/100;
    $tax = (float)$_POST['tax']/100;
    $payoutmode = $_POST['payout'];
    $supporters = $_POST['supporters'];
    $supp_share = $_POST['supp_share']/100;
    if (strpos($_POST['itemlist'], 'erbeutet')) {
        $lang = 'de';
        $regexp = "/[0-9:]*\s(?<name>.*)\shat\s(?<amount>[0-9.]*)\sx\s(?<ore>[A-Za-z\s0-5'\-]*)\*/";
    } else {
        $lang = 'en';
        $regexp = "/[0-9:]*\s(?<name>.*)\shas\slootet\s(?<amount>[0-9.]*)\sx\s(?<ore>[A-Za-z\s0-5'\-]*)/";
    }
    switch($_POST['order_type']) {
        case 'buy_perc':
            $order_type = "buy";
            $use_percentile = true;
            break;
        case 'sell_perc':
            $order_type = "sell";
            $use_percentile = true;
            break;
        case 'buy_high':
            $order_type = "buy";
            $use_percentile = false;
            break;
        case 'sell_low':
            $order_type = "sell";
            $use_percentile = false;
    }
    $hub = $_POST['hub'];
    $pilots = array();
    $lines = (explode(PHP_EOL, $_POST['itemlist']));
    foreach ($lines as $l) {
        $matches = array();
        preg_match($regexp, $l, $matches);
        if (isset($matches['ore']) && isset($matches['name']) && isset($matches['amount'])) {
            if (!isset($pilots[$matches['name']])) {
                $pilots[$matches['name']] = array();
            }
            $amount = str_replace(array('.',','), '', $matches['amount']);
            if (!isset($pilots[$matches['name']][$matches['ore']])) {
                $pilots[$matches['name']][$matches['ore']] = $amount;
            } else {
                $pilots[$matches['name']][$matches['ore']] += $amount;
            }
        }
    }
    $ores = ( array_unique(array_reduce(array_map('array_keys',$pilots),'array_merge',[])) );
    $qry = DB::getConnection();
    $sqlitems = [];
    $quantities = [];
    $sql="SELECT typeID, typeName, portionSize, volume FROM invTypes WHERE typeName='".implode("' OR typeName='", $ores)."'";
    $result = $qry->query($sql);
    $list = array();
    $allmaterials = array();
    if($result->num_rows) {
        while ($row = $result->fetch_assoc()) {
            $list[$row['typeID']] = ['name' => utf8_encode($row['typeName']), 'mats' => array(), 'size' => $row['portionSize'], 'volume' => $row['volume']];
            $allmaterials[$row['typeID']] = utf8_encode($row['typeName']);
        }
    }

    if (count($list)) {
        $sql="SELECT itm.typeID, itm.materialTypeID, itm.quantity, it.typeName AS materialName FROM invTypeMaterials AS itm INNER JOIN invTypes AS it ON it.typeID = itm.materialTypeID WHERE itm.typeID=".implode(" OR itm.typeID=", array_keys($list));
        $result = $qry->query($sql);
        if($result->num_rows) {
            while ($row = $result->fetch_assoc()) {
                $list[$row['typeID']]['mats'][$row['materialTypeID']] = ['name' => utf8_encode($row['materialName']), 'qty' => (int)$row['quantity']];
                $allmaterials[$row['materialTypeID']] = utf8_encode($row['materialName']);
            }
        }
    }
    ksort($allmaterials);
    $qryitems = array_filter(array_merge(array_keys($allmaterials), array_keys($list)));
    $esiapi = new ESIAPI();
    $marketapi = $esiapi->getApi('Market');
    $promises = [];
    switch ($hub) {
        case "jita":
            $station_id = 60003760;
            $system_id = 30000142;
            $region_id = 10000002;
            break;
        case "amarr":
            $station_id = 60008494;
            $system_id = 30002187;
            $region_id = 10000043;
            break;
        case "rens":
            $station_id = 60004588;
            $system_id = 30002510;
            $region_id = 10000030;
            break;
        case "dodixie":
            $station_id = 60011866;
            $system_id = 30002659;
            $region_id = 10000032;
            break;
        case "hek":
            $station_id = 60005686;
            $system_id = 30002053;
            $region_id = 10000042;
            break;
    }
    $jumpcount = array();
    if ($order_type == "buy") {
        $sql="SELECT itemID, solarSystemID, groupID FROM mapDenormalize WHERE regionID = ".$region_id;
        $result = $qry->query($sql);
        $allstations = array();
        if($result->num_rows) {
            while ($row = $result->fetch_assoc()) {
                if (!isset($allstations[$row['solarSystemID']])) {
                    $allstations[$row['solarSystemID']] = ['stations' => array(), 'jumps' => null];
                }
                if ($row['groupID'] == 15) {
                    $allstations[$row['solarSystemID']]['stations'][] = $row['itemID'];
                }
            }
        }
        $lookups = count($allstations);
        $i = 1;
        $allstations[$system_id]['jumps'] = 0;
        $lastsystems = array($system_id);
        $prev_last = array(0);
        do {
            $sql="SELECT toSolarSystemID FROM mapSolarSystemJumps
                  WHERE toRegionID = ".$region_id."
                  AND (fromSolarSystemID = ".implode(" OR fromSolarSystemID = ", $lastsystems).")
                  AND NOT (toSolarSystemID = ".implode(" OR toSolarSystemID = ", $prev_last).")";
            $result = $qry->query($sql);
            $prev_last = $lastsystems;
            $lastsystems = array();
            if($result->num_rows) {
                while ($row = $result->fetch_assoc()) {
                    $lastsystems[] = $row['toSolarSystemID'];
                    if(isset($allstations[$row['toSolarSystemID']]) && $allstations[$row['toSolarSystemID']]['jumps'] === null) {
                        $allstations[$row['toSolarSystemID']]['jumps'] = $i;
                        $lookups -= 1;
                    }
                }
            }
            $result->close();
            $i += 1;
        } while ($i <= 40 && $lookups && count($lastsystems));
        $jumpcount = array();
        foreach ($allstations as $s) {
            foreach ($s['stations'] as $st) {
                $jumpcount[$st] = $s['jumps'];
            }
        }
    }
    $p = 1;
    foreach($qryitems as $i) {
        $promises[] = $marketapi->getMarketsRegionIdOrdersAsync($order_type, $region_id, "tranquility", null, $p, $i);
    }
    $responses = GuzzleHttp\Promise\settle($promises)->wait();
    $prices = array_fill_keys($qryitems, 0);
    $structurelookups = array();
    foreach($responses as $response) {
        if ($response['state'] == 'fulfilled') {
            if (!isset($response['value'][0]) || $response['value'][0] === null) {
                continue;
            }
            foreach($response['value'] as $r) {
                if (!isset($jumpcount[$r->getLocationId()]) && !isset($structurelookups[$r->getLocationId()])) {
                    $structurelookups[$r->getLocationId()] = 100;
                }
            }
        }
    }
    if (count($structurelookups)) {
        $promises2 = array();
        $esisso = new ESISSO(null, $_SESSION['characterID']);
        $esiapi->setAccessToken($esisso->getAccessToken('esi-universe.read_structures.v1'));
        $universeapi = $esiapi->getApi('Universe');
        foreach (array_keys($structurelookups) as $id) {
            $promises2[$id] = $universeapi->getUniverseStructuresStructureIdAsync($id, 'tranquility');
        }
        $responses2 = GuzzleHttp\Promise\settle($promises2)->wait();
        foreach ($responses2 as $id => $response2) {
            if($response2['state'] == 'fulfilled') {
                $structurelookups[$id] = $allstations[$response2['value']->getSolarSystemId()]['jumps'];
            }
        }
        $jumpcount = $jumpcount + $structurelookups;
    }
    foreach($responses as $response) {
        if ($response['state'] == 'fulfilled') {
            if (!isset($response['value'][0]) || $response['value'][0] == null) {
                continue;
            }
            $typeid = $response['value'][0]->getTypeId();
            $orders = [];
            $totalvol = 0;
            foreach($response['value'] as $r) {
                if ( 
                    ($order_type == "sell" && $r->getLocationId() == $station_id) ||
                    ($order_type == "buy" && 
                        (
                            $r->getLocationId() == $station_id ||
                            $r->getRange() == 'region' ||
                            ( $r->getRange() == '1' && $jumpcount[$r->getLocationId()] <= 1 ) ||
                            ( $r->getRange() == '2' && $jumpcount[$r->getLocationId()] <= 2 ) ||
                            ( $r->getRange() == '3' && $jumpcount[$r->getLocationId()] <= 3 ) ||
                            ( $r->getRange() == '4' && $jumpcount[$r->getLocationId()] <= 4 ) ||
                            ( $r->getRange() == '5' && $jumpcount[$r->getLocationId()] <= 5 ) ||
                            ( $r->getRange() == '10' && $jumpcount[$r->getLocationId()] <= 10 ) ||
                            ( $r->getRange() == '20' && $jumpcount[$r->getLocationId()] <= 20 ) ||
                            ( $r->getRange() == '30' && $jumpcount[$r->getLocationId()] <= 30 ) ||
                            ( $r->getRange() == '40' && $jumpcount[$r->getLocationId()] <= 40 ) 
                        ) 
                    ) 
                ) {
                    if (isset($orders[$r->getPrice()*100])) {
                        $orders[$r->getPrice()*100] += $r->getVolumeRemain();
                    } else {
                        $orders[$r->getPrice()*100] = $r->getVolumeRemain();
                    }
                }
            }
            if ($order_type == "buy") {
                krsort($orders);
            } else {
                ksort($orders);
            }
            if (count($orders) > 5) {
                $orders = array_slice($orders, 0, (int)floor(count($orders)*0.98), true);
            }
            if ($use_percentile) {
                $totalvol = array_sum($orders);
                $running = 0;
                $prev_perc = 0;
                $prev_price = 0;
                foreach($orders as $p => $q) {
                    $running += $q;
                    $perc = ($running/$totalvol) * 100;
                    if ($perc >= 5 || ($perc > 5 && $prev_perc == 0) ) {
                        $prices[$typeid] = (int)round($p, 0);
                        break;
                    } elseif ($perc > 5) {
                        $ratio = ( $perc/($perc-$prev_perc) );
                        $prices[$typeid] = (int)round($p*$ratio + $prev_price*(1-$ratio), 0);
                        break;
                    }
                    $prev_perc = $perc;
                    $prev_price = $p;
                }
            } else {
                reset($orders);
                $prices[$typeid] = (int)round(key($orders), 0);    
            }
        }
    }
    $charids = EVEHELPERS::esiCharNamesToIds(array_keys($pilots));
    $tableinfo = array();
    $totaliskraw = 0;
    $totaliskrep = 0;
    $totalvol = 0;
    foreach ($pilots as $pn => $mined) {
        if (in_array($charids[$pn], $removed)) {
            continue;
        }
        $tableinfo[$charids[$pn]] = array('name' => $pn, 'volmined' => (float)0, 'iskmined' => (float)0, 'iskreprocessed' => (float)0, 'details' => array());
        foreach ($mined as $mn => $qty) {
            $mid = array_flip($allmaterials)[$mn];
            $value = $qty * $prices[$mid];
            $tableinfo[$charids[$pn]]['iskmined'] += $value;
            $volume = $qty * $list[$mid]['volume'];
            $tableinfo[$charids[$pn]]['volmined'] += $volume;
            $valuerep = 0;
            foreach ($list[$mid]['mats'] as $m => $i) {
                $valuerep += round($qty/$list[$mid]['size'] * $rep_eff * $i['qty'] * $prices[$m]);
            }
            $tableinfo[$charids[$pn]]['iskreprocessed'] += $valuerep;
            $tableinfo[$charids[$pn]]['details'][] = array('id' => $mid, 'name' => $mn, 'value' => $value, 'valuerep' => $valuerep, 'volume' => $volume, 'amount' => $qty);
        }
        $totaliskraw += $tableinfo[$charids[$pn]]['iskmined'];
        $totaliskrep += $tableinfo[$charids[$pn]]['iskreprocessed'];
        $totalvol += $tableinfo[$charids[$pn]]['volmined'];
    }
    foreach ($tableinfo as $i => $t) {
        switch ($payoutmode) {
            case "volrep":
                $net = $t['volmined']/$totalvol * $totaliskrep;
                $supp_amount = $totaliskrep * $supp_share;
                $payoutmode_txt = 'by volume, reprocessed value';
                break;
            case "volraw":
                $net = $t['volmined']/$totalvol * $totaliskraw;
                $supp_amount = $totaliskraw * $supp_share;
                $payoutmode_txt = 'by volume, ore value';
                break;
            case "repisk":
                $net = $t['iskreprocessed'];
                $supp_amount = $totaliskrep * $supp_share;
                $payoutmode_txt = 'individual reprocessed value';
                break;
            case "rawisk":
                $net = $t['iskmined'];
                $supp_amount = $totaliskraw * $supp_share;
                $payoutmode_txt = 'individual ore value';
                break;
        }
        $net -= $net * ($supporters * $supp_share);
        $tableinfo[$i]['tax'] = round($net * $tax);
        $tableinfo[$i]['payout'] = round($net * (1- $tax));
    }
    $supp_payout = round($supp_amount * (1- $tax));
    $supp_tax = round($supp_amount * $tax);
    $html = "<ul class='nav nav-pills'>
                 <li class='active'><a data-toggle='pill' href='#meltcontent'>Result</a></li>
                 <li><a data-toggle='pill' href='#usedprices'>Used prices</a></li>
                 <li><a data-toggle='pill' href='#inputtxt'>Input</a></li>
             </ul>
             <div class='tab-content'>
             <div id='meltcontent' class='tab-pane fade in active'>
                 <div class='col-xs-12'>
                     <h5>Result (".$order_type." orders, payout ".$payoutmode_txt."):</h5>
                     <table class='jdatatable datatable table small responsive table-striped table-hover'>
                         <thead>
                             <tr>
                                 <th class='no-sort num-col'>
                                 <th data-priority='1'>Pilot</th>
                                 <th class='no-sort'></th>
                                 <th class='volume num sumup' data-priority='1'>Volume</th>
                                 <th class='price num sumup'>ISK (Ore)</th>
                                 <th class='price num sumup'>ISK (Reprocessed)</th>
                                 <th class='price num sumup'>Tax</th>
                                 <th class='payout num sumup' data-priority='1'>Payout</th>
                                 <th class='no-sort'></th>
                             </tr>
                         </thead>
                         <tfoot>
                             <tr>
                                <th colspan='2' style='text-align:right'>Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class='text-danger'></th>
                                <th class='text-success'></th>
                                <th></th>
                             </tr>
                         </tfoot>";
    foreach($tableinfo as $i => $t) {
        $html .= "       <tr id='".$i."' data-child-value='".json_encode($t['details'])."'>
                             <td class='num-col'><img class='img-rounded' height='28px' src='https://images.evetech.net/characters/".$i."/portrait?size=32'></td>
                             <td class='name'>".$t['name']."</td>
                             <td class='details-control text-muted' title='Toggle details'></td>
                             <td>".round($t['volmined'])."</td>
                             <td>".($t['iskmined']/100)."</td>
                             <td>".($t['iskreprocessed']/100)."</td>
                             <td class='text-danger'>".($t['tax']/100)."</td>
                             <td class='text-success'>".($t['payout']/100)."</td>
                             <td><button type='button' title='Remove pilot' class='btn btn-link btn-xs' onclick='deluser(this)'><span class='glyphicon glyphicon-trash'></span></button></td>
                         </tr>";
    }
    for($i = 0; $i < $supporters; ++$i) {
        $html .= "       <tr id='".$i."'>
                             <td class='num-col'><img class='img-rounded' height='28px' src='https://images.evetech.net/characters/1/portrait?size=32'></td>
                             <td class='name'>Supporter ".($i+1)."</td>
                             <td class='text-muted'></td>
                             <td>0</td>
                             <td>0</td>
                             <td>0</td>
                             <td class='text-danger'>".($supp_tax/100)."</td>
                             <td class='text-success'>".($supp_payout/100)."</td>
                             <td></td>
                         </tr>";
    }
    $html .= "       </table>
                 </div>
             </div>";

    $html .= "<div id='usedprices' class='tab-pane fade col-xs-12'>
                 <div class='col-xs-12 col-sm-10 col-md-8 col-lg-6'>
                     <h5>Used Material prices (".$order_type." orders):</h5>
                     <table class='table small responsive table-striped table-hover'>
                         <thead>
                             <tr>
                                 <th>Material</th><th class='num-col'>Price</th>
                             </tr>
                         </thead>";
    foreach($allmaterials as $i => $n) {
        $html .= "<tr><td><img src='https://imageserver.eveonline.com/Type/".$i."_32.png' style='height:24px'> ".$n."</td><td class='num-col'>".number_format($prices[$i]/100, 2,".",",")." ISK</td></tr>";
    }
    $html .= "       </table>
                 </div>
             </div>";
    $page->addBody($html);
    $page->addBody("<div id='inputtxt' class='tab-pane fade'><div style='width: 100%; height: 15px'/>");
    $page->addBody(getForm($_POST));
    $page->addBody("</div></div>");
} else {
    $page->addBody(getForm());
}
$page->addFooter(getScriptFooter());
$page->setBuildTime(number_format(microtime(true) - $start_time, 3));
$page->display();
exit;
?>
